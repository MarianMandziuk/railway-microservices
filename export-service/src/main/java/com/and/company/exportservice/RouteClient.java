package com.and.company.exportservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient("routeservices")
public interface RouteClient {

    @PostMapping("/routes")
    @ResponseStatus(HttpStatus.CREATED)
    Route createRoute(@RequestParam @Validated Route routeDto);

    @GetMapping("/routes/{number}")
    @ResponseStatus(HttpStatus.OK)
    Route getRoute(@PathVariable int number);

    @GetMapping("/routes/{id}")
    @ResponseStatus(HttpStatus.OK)
    Route getRouteById(@PathVariable long id);

    @PutMapping("/routes/{number}")
    @ResponseStatus(HttpStatus.OK)
    Route updateRoute(@PathVariable int number,@RequestParam @Validated Route routeDto);

    @DeleteMapping("/routes/{number}")
    ResponseEntity<Void> deleteRoute(@PathVariable int number);
}
