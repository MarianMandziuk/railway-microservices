package com.and.company.ticketsservice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/tickets")
@RequiredArgsConstructor
public class TicketController {

    private final TicketRepository ticketRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Ticket createTicket(@RequestBody @Validated TicketDto ticketDto) {
        log.info("Create ticket : {}", ticketDto);
        return ticketRepository.save(Ticket.builder()
                .arrival(ticketDto.getArrival())
                .departure(ticketDto.getDeparture())
                .fromOf(ticketDto.getFromOf())
                .to(ticketDto.getTo())
                .seat(ticketDto.getSeat())
                .build());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Ticket getTicket(@PathVariable long id) {
        log.info("Getting ticket by id: {}.", id);
        return ticketRepository.findById(id)
                .orElseThrow(() -> new TicketNotFoundException("There is no such ticket."));
    }

    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Ticket> getAllTickets() {
        log.info("Getting all tickets");
        List<Ticket> ticketList = new ArrayList<>();
        ticketRepository.findAll().forEach(ticketList::add);
        return ticketList;
    }


    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public List<Ticket> getUserTickets(@PathVariable long userId) {
        log.info("Getting ticket by id: {}.", userId);
        return ticketRepository.findByUserId(userId);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Ticket updateTicket(@PathVariable long id, @RequestBody @Validated TicketDto ticketDto) {
        log.info("Updating ticket by id : {} ", id);
        if (!ticketRepository.existsById(id)) {
            throw new TicketNotFoundException("There is no such ticket.");
        }
        return ticketRepository.save(Ticket.builder()
                .arrival(ticketDto.getArrival())
                .departure(ticketDto.getDeparture())
                .fromOf(ticketDto.getFromOf())
                .to(ticketDto.getTo())
                .seat(ticketDto.getSeat())
                .build());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTicket(@PathVariable long id) {
        log.info("Deleting ticker by id : {} ", id);
        Ticket ticket = ticketRepository.findById(id)
                .orElseThrow(() -> new TicketNotFoundException("There is no such ticket."));
        ticketRepository.delete(ticket);
        return ResponseEntity.noContent().build();
    }
}
