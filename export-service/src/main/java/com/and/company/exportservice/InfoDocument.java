package com.and.company.exportservice;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InfoDocument {
    private long userId;
    private String firstName;
    private String lastName;
    private String email;
    private List<Ticket> ticketList;
    private List<Route> routeList;
}
