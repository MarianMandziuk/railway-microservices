package com.and.company.managementservice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/route-web")
@RequiredArgsConstructor
@Slf4j
public class RouteWebService {
    private final RouteClient routeClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RouteDto createRoute(@RequestBody @Validated RouteDto route) {
        log.info("RouteWebService: createRoute - {}", route);
        return routeClient.createRoute(route);
    }

    @GetMapping("/{number}")
    @ResponseStatus(HttpStatus.OK)
    public RouteDto getRoute(@PathVariable int number) {
        log.info("RouteWebService: getRoute - {}", number);
        return routeClient.getRoute(number);
    }

    @PutMapping("/{number}")
    @ResponseStatus(HttpStatus.OK)
    public RouteDto updateRoute(@PathVariable int number, @RequestBody @Validated RouteDto route) {
        log.info("RouteWebService: updateRoute - {} - {}", number, route);
        return routeClient.updateRoute(number, route);
    }

    @DeleteMapping("/{number}")
    public ResponseEntity<Void> deleteRoute(@PathVariable int number) {
        log.info("RouteWebService: deleteRoute - {}", number);
        return routeClient.deleteRoute(number);
    }
}
