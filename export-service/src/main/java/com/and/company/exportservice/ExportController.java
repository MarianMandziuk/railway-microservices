package com.and.company.exportservice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/export-service")
@RequiredArgsConstructor
@Slf4j
public class ExportController {

    private final RouteClient routeClient;

    private final TicketClient ticketClient;

    private final UserClient userClient;

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public InfoDocument generateDocument(@PathVariable long userId) {
        log.info("Generating document for user with id : {}", userId);
        User user = userClient.getUser(userId);
        InfoDocument infoDocument = new InfoDocument();
        infoDocument.setUserId(userId);
        infoDocument.setEmail(user.getEmail());
        infoDocument.setFirstName(user.getFirstName());
        infoDocument.setLastName(user.getLastName());
        List<Ticket> ticketList = ticketClient.getUserTickets(userId);
        infoDocument.setTicketList(ticketList);
        List<Route> routeList = new ArrayList<>();
        ticketList.forEach(t ->
                routeList.add(routeClient.getRouteById(t.getRouteId())));
        infoDocument.setRouteList(routeList);
        return infoDocument;
    }
}
