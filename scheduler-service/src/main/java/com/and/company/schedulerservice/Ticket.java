package com.and.company.schedulerservice;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

@Data
public class Ticket {
    private Long id;

    @NotBlank
    private String fromOf;

    @NotBlank
    private String to;

    private LocalDateTime departure;

    private LocalDateTime arrival;

    @Min(1)
    private Integer seat;

    private Long routeId;

    private Long userId;

    private boolean booked;
}
