package com.and.company.exportservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient("userservices")
public interface UserClient {
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    User getUser(@PathVariable long id);

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    User createUser(@RequestParam @Validated UserDto userDto);

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    User updateUser(@PathVariable long id,
                           @RequestParam @Validated UserDto userDto);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteUser(@PathVariable long id);
}
