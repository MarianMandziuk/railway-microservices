package com.and.company.routeservice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/routes")
@RequiredArgsConstructor
public class RouteController {

    private final RouteRepository routeRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Route createRoute(@RequestBody @Validated RouteDto routeDto) {
        log.info("Create route : {}", routeDto);
        Optional<Route> opRoute = routeRepository.findByNumber(routeDto.getNumber());
        if (opRoute.isPresent()) {
            throw new RoutePresentException("Such as route present in db.");
        }
        return routeRepository.save(Route.builder()
                .availableSeats(routeDto.getAvailableSeats())
                .departure(routeDto.getDeparture())
                .number(routeDto.getNumber())
                .fromVi(routeDto.getFromVi())
                .toVi(routeDto.getToVi())
                .build());
    }

    @GetMapping("/{number}")
    @ResponseStatus(HttpStatus.OK)
    public Route getRoute(@PathVariable int number) {
        log.info("Getting route by number: {}.", number);
        return routeRepository.findByNumber(number)
                .orElseThrow(() -> new RouteNotFoundException("There is no such a route with number : " + number));
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Route getRouteById(@PathVariable long id) {
        log.info("Getting route by id: {}.", id);
        return routeRepository.findById(id)
                .orElseThrow(() -> new RouteNotFoundException("There is no such a route with id : " + id));
    }


    @PutMapping("/{number}")
    @ResponseStatus(HttpStatus.OK)
    public Route updateRoute(@PathVariable int number, @RequestBody @Validated RouteDto routeDto) {
        log.info("Updating route by number : {} ", number);
        routeRepository.findByNumber(number)
                .orElseThrow(() -> new RouteNotFoundException("There is no such a route with number : " + number));
        Route route = Route.builder()
                .availableSeats(routeDto.getAvailableSeats())
                .departure(routeDto.getDeparture())
                .number(routeDto.getNumber())
                .build();
        return routeRepository.save(route);
    }

    @DeleteMapping("/{number}")
    public ResponseEntity<Void> deleteRoute(@PathVariable int number) {
        log.info("Deleting route by number : {} ", number);
        Route route = routeRepository.findByNumber(number)
                .orElseThrow(() -> new RouteNotFoundException("There is no such a route with number : " + number));
        routeRepository.delete(route);
        return ResponseEntity.noContent().build();
    }


}
