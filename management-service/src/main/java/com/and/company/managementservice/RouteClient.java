package com.and.company.managementservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@FeignClient("routeservices")
public interface RouteClient {

    @PostMapping("/routes")
    @ResponseStatus(HttpStatus.CREATED)
    public RouteDto createRoute(@RequestBody @Validated RouteDto routeDto);

    @GetMapping("/routes/{number}")
    @ResponseStatus(HttpStatus.OK)
    public RouteDto getRoute(@PathVariable int number);

    @GetMapping("/routes/{id}")
    @ResponseStatus(HttpStatus.OK)
    public RouteDto getRouteById(@PathVariable long id);

    @PutMapping("/routes/{number}")
    @ResponseStatus(HttpStatus.OK)
    public RouteDto updateRoute(@PathVariable int number, @RequestBody @Validated RouteDto routeDto);

    @DeleteMapping("/routes/{number}")
    public ResponseEntity<Void> deleteRoute(@PathVariable int number);
}
