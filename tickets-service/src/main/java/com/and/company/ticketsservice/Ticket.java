package com.and.company.ticketsservice;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "ticket")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Ticket {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String fromOf;

    private String to;

    private LocalDateTime departure;

    private LocalDateTime arrival;

    private Integer seat;

    private Long routeId;

    private Long userId;

    private boolean booked;

}
