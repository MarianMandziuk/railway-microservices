package com.and.company.routeservice;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class RouteDto {
    private Long id;

    @Min(10000)
    @Max(99999)
    private Integer number;

    private LocalDateTime departure;

    @Min(0)
    private Integer availableSeats;

    private String fromVi;

    private String toVi;

    @NotEmpty
    private List<String> station;
}
