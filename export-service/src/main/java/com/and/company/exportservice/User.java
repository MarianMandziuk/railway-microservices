package com.and.company.exportservice;

import lombok.Data;

@Data
public class User {
    private Long id;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

}
