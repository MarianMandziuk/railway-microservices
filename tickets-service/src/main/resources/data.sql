INSERT INTO ticket (id, from_of, to, departure, arrival, seat, route_id, user_id, booked)
VALUES (1, 'Lviv', 'Chernivtsi', '2021-03-22T10:00:00', '2021-03-22T17:35:00', 43, 31, 1, false);
INSERT INTO ticket (id, from_of, to, departure, arrival, seat, route_id, user_id, booked)
VALUES (2, 'Lviv', 'Ivan-Frankivsk', '2021-04-22T10:00:00', '2021-04-22T15:35:00', 11, 10, 1, true);
INSERT INTO ticket (id, from_of, to, departure, arrival, seat, route_id, user_id, booked)
VALUES (3, 'Halych', 'Vorohta', '2021-05-22T10:00:00', '2021-05-22T17:35:00', 1, 1, 1, false);