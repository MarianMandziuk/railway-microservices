package com.and.company.managementservice;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class RouteDto {
    private long id;

    @Min(10000)
    @Max(99999)
    private int number;

    private LocalDateTime departure;

    @Min(0)
    private int availableSeats;

    private String fromVi;

    private String toVi;

    @NotEmpty
    private List<String> station;
}
