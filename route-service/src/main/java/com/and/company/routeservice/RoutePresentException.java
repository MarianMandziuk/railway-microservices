package com.and.company.routeservice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code=HttpStatus.CONFLICT)
public class RoutePresentException extends RuntimeException {
    public RoutePresentException(String message) {
        super(message);
    }
}
