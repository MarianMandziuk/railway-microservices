package com.and.company.schedulerservice;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Configuration
@EnableScheduling
public class Scheduler {

    private final TicketClient ticketClient;

    @Scheduled(fixedDelay = 5000 )
    public void removeUnassignedTickets() {
        log.info("Scheduler --> removeUnassignedTickets");
        List<Ticket> ticketList = ticketClient.getAllTickets();
        for (Ticket t : ticketList) {
            if (t.isBooked() && t.getUserId() == null) {
                ticketClient.deleteTicket(t.getId());
            }
        }
    }
}
