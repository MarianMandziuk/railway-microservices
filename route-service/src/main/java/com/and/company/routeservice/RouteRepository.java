package com.and.company.routeservice;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RouteRepository extends CrudRepository<Route, Long> {

    Optional<Route> findByNumber(int number);

}
