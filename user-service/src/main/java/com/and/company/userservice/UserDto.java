package com.and.company.userservice;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
  private long id;

  private String firstName;

  @NotNull(message = "should by not null")
  private String lastName;

  @NotNull
  @Email
  private String email;

  private String password;

  private String repeatPassword;

}
