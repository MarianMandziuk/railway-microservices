package com.and.company.schedulerservice;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient("ticketservices")
public interface TicketClient {
    @PostMapping
    Ticket createTicket(@RequestBody @Validated Ticket ticket);

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    Ticket getTicket(@PathVariable long id);

    @GetMapping("tickets/all")
    @ResponseStatus(HttpStatus.OK)
    List<Ticket> getAllTickets();

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    List<Ticket> getUserTickets(@PathVariable long userId);

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    Ticket updateTicket(@PathVariable long id, @RequestBody @Validated Ticket ticket);

    @DeleteMapping("/{id}")
    ResponseEntity<Void> deleteTicket(@PathVariable long id);
}
