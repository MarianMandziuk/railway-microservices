package com.and.company.routeservice;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "route")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Route {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(unique = true)
    private Integer number;

    private LocalDateTime departure;

    private Integer availableSeats;

    private String fromVi;

    private String toVi;

    @ToString.Exclude
    @OneToMany
    private List<Station> station;
}
